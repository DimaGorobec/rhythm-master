let burger = document.querySelector('.g-menu');//бругер меню
let navigationRes = document.querySelector('.main-screen') //пустой блок
	burger.addEventListener('click', function () {
		let nav = document.querySelector('.nav-none');
		let navMob = nav.cloneNode(true);
		navMob.classList.remove('nav-none')
		navMob.classList.add('header__burger_show')
		navMob.classList.add('animated')
		navMob.classList.add('bounceInDown')
		navigationRes.appendChild(navMob)
		burger.classList.add('nav-none')
		let closeNav = document.createElement('div');
		closeNav.innerHTML = '<i class="fas fa-times"></i>';
		closeNav.classList.add('close')
		navigationRes.appendChild(closeNav)
		closeNav.addEventListener('click',function (){	
		navMob.classList.add('nav-none');
		closeNav.classList.add('nav-none')
		burger.classList.remove('nav-none')
})
	})
let partfolioGalary = document.querySelectorAll('.portfolio-galery >div')
console.log(partfolioGalary)
let brandingShow = document.querySelector('#branding')
console.log(brandingShow)
brandingShow.addEventListener('click',function() {
	for (let i=0;i<partfolioGalary.length; i++) {
		if (partfolioGalary[i].classList.contains('gallarynone')) {
			partfolioGalary[i].classList.remove('gallarynone')
	}
}
	for (let i=0; i<partfolioGalary.length; i++) {
		if (!partfolioGalary[i].classList.contains('branding')) {
			partfolioGalary[i].classList.add('gallarynone')
		}
	}
})
function design() {
	for (let i=0;i<partfolioGalary.length; i++) {
		if (partfolioGalary[i].classList.contains('gallarynone')) {
			partfolioGalary[i].classList.remove('gallarynone')
	}
}
	for (let i=0; i<partfolioGalary.length; i++) {
		if (!partfolioGalary[i].classList.contains('design')) {
			partfolioGalary[i].classList.add('gallarynone')
		}
	}
}
function photography() {
	for (let i=0;i<partfolioGalary.length; i++) {
		if (partfolioGalary[i].classList.contains('gallarynone')) {
			partfolioGalary[i].classList.remove('gallarynone')
	}
}
	for (let i=0; i<partfolioGalary.length; i++) {
		if (!partfolioGalary[i].classList.contains('photography')) {
			partfolioGalary[i].classList.add('gallarynone')
		}
	}
}
function allWorks() {
	for (let i=0;i<partfolioGalary.length; i++) {
		if (partfolioGalary[i].classList.contains('gallarynone')) {
			partfolioGalary[i].classList.remove('gallarynone')
		}
	}
}


// старт слайдер
let slideIndex = 1;
showSlides(slideIndex)
function plusSlide (n) {
	showSlides(slideIndex +=n);
}
function currentSlide (n) {
	showSlides(slideIndex = n)
}

function showSlides (n) {
	let i;
	let slides = document.querySelectorAll('.TESTIMONIALS');
	if ( n >slides.length) {
		slideIndex = 1;
	}
	if ( n < 1) {
		slideIndex=slides.length
	}
	for (i = 0; i<slides.length; i++){
		slides[i].style.display= "none";
	}
	slides[slideIndex-1].style.display = "block";
}

let wrapper = document.querySelector('.conteiner_map__wrapper')
let icon = document.querySelector('.conteiner_map__wrapper > img')
let text = document.querySelector('.conteiner_map__wrapper > p')
wrapper.onclick = function () {
	wrapper.style.height = 55 + 'px';
	text.style.marginTop = 10 + 'px';
	 function timeDelete() {
       icon.style.height = '0';
}
    setTimeout(timeDelete,300);
    if (wrapper.style.height === '55px')   {
    	wrapper.onclick = function () {
    		wrapper.style.height = 410 + 'px'
    	}
    	
    }
}
console.log(icon)


//START SECTION QUOTE DROPDOVN
let conteinerQuote = document.querySelector('.quote');
let conteinerSecondText = document.querySelector('.second_text');
let conteinerThirdText = document.querySelector('.third_text');
let quote = document.querySelector('.first_text');
let avtorName = document.querySelector('.avtor_name');
function branding() {
	heightNone()
	function timeDelete() {
	  quote.innerHTML = 'A brand for a company is like a reputation for a person. You earn reputation by trying to do hard things well.';
      avtorName.innerHTML ='— STEVE KRUG';
      conteinerSecondText.innerHTML = 'Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum volutpat nibh, accumsan purus. Lorem ipsum dolor sit semper amet, consectetur adipiscing elit. In maximus ligula metus pellentesque mattis.';
      conteinerThirdText.innerHTML = 'Donec vel ultricies purus. Nam dictum sem, ipsum aliquam . Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor. Integer lectus. Praesent sed nisi eleifend, fermentum orci amet, iaculis libero.';
      heightShow()
	}
	setTimeout(timeDelete,900);
}
function web_design() {
	heightNone()
	function timeDelete() {
      quote.innerHTML = 'It doesn’t matter how many times I have to click, as long as each click is a mindless, unambiguous choice.';
      avtorName.innerHTML ='— STEVE KRUG';
      conteinerSecondText.innerHTML = 'Cras mi tortor, laoreet id ornare et, accumsan non magna. Maecenas vulputate accumsan velit. Curabitur a nulla ex. Nam a tincidunt ante. Vitae gravida turpis. Vestibulum varius nulla non nulla scelerisque tristique.';
      conteinerThirdText.innerHTML = 'Mauris id viverra augue, eu porttitor diam. Praesent faucibus est a interdum elementum. Nam varius at ipsum id dignissim. Nam a tincidunt ante lorem. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor.';
      heightShow()
	}
	setTimeout(timeDelete,900);
}
function Graphic_Design() {
	heightNone()
	function timeDelete() {
      quote.innerHTML = 'Never fall in love with an idea. They’re whores. If the one you’re with isn’t doing the job, there’s always another.';
      avtorName.innerHTML ='—  JEFF BEZOS';
      conteinerSecondText.innerHTML = 'Lorem ipsum dolor sit semper amet, consectetur adipiscing elit. In maximus ligula metus pellentesque mattis. Donec vel ultricies purus. Nam dictum sem, ipsum aliquam . Praesent sed nisi eleifend, fermentum orci amet, iaculis libero.';
      conteinerThirdText.innerHTML = 'Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor. Integer lectus. Etiam sit amet fringilla lacus. Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum volutpat nibh, accumsan purus.';
      heightShow()
	}
	setTimeout(timeDelete,900);
}
function Development() {
	heightNone()
	function timeDelete() {
      quote.innerHTML = 'All that is valuable in human society depends upon the opportunity for development accorded the individual.';
      avtorName.innerHTML ='—  ALBERT EINSTEIN';
      conteinerSecondText.innerHTML = 'Fusce hendrerit vitae nunc id gravida. Donec euismod quis ante at mattis. Mauris dictum ante sit amet enim interdum semper. Vestibulum odio justo, faucibus et dictum eu, malesuada nec neque. Maecenas volutpat, diam enim sagittis.';
      conteinerThirdText.innerHTML = 'Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor. Sed id dolor consectetur fermentum volutpat nibh, accumsan purus. Lorem ipsum dolor sit semper amet, consectetur adipiscing elit. Inmed maximus ligula metus pellentesque.';
     heightShow()
	}
	setTimeout(timeDelete,900);
}
function Photography() {
	heightNone()
	function timeDelete() {
      quote.innerHTML = 'Photography is the simplest thing in the world, but it is incredibly complicated to make it really work.';
      avtorName.innerHTML ='—  MARTIN PARR';
      conteinerSecondText.innerHTML = 'Donec vel ultricies purus. Nam dictum sem, ipsum aliquam . Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor. Integer lectus. Praesent sed nisi eleifend, fermentum orci amet, iaculis libero.';
      conteinerThirdText.innerHTML = 'Maecenas volutpat, diam enim sagittis quam, id porta quam. Sed id dolor consectetur fermentum volutpat nibh, accumsan purus. Lorem ipsum dolor sit semper amet, consectetur adipiscing elit. In maximus ligula metus pellentesque mattis.';
      heightShow()
	}
	setTimeout(timeDelete,900);
}

function heightNone() {
	conteinerQuote.style.height = 0;
	conteinerSecondText.style.height = 0;
	conteinerThirdText.style.height = 0;
}
function heightShow() {
	conteinerQuote.style.height = 200 + 'px';
    conteinerSecondText.style.height = 200 + 'px';
    conteinerThirdText.style.height = 200 + 'px';
}